﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleShop.Basket.WebHost.DTO.Models
{
    public class BasketResponse
    {
        public Guid BasketId { get; set; }
        public Guid UserId { get; set; }

        public List<BasketItemResponse> BasketItems { get; set; }
    }
}
