﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleShop.Basket.WebHost.DTO.Models
{
    public class BasketItemUpdateRequest
    {
        [Required]
        public Guid BasketItemId { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int Count { get; set; }
    }
}
