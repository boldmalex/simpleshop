﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleShop.Basket.WebHost.DTO.Models
{
    public class BasketItemRequest
    {
        [Required]
        public Guid UserId { get; set; }
        [Required]
        public Guid ProductId { get; set; }
        [Required]
        [MinLength(1)]
        public string ProductName { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int Count { get; set; }
        [Required]
        [Range(1, double.MaxValue)]
        public decimal Price { get; set; }
    }
}
