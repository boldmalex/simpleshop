using AutoFixture;
using AutoFixture.AutoMoq;
using Moq;
using SimpleShop.Basket.Core.Domain;
using SimpleShop.Basket.Core.Repositories;
using SimpleShop.Basket.WebHost.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using FluentAssertions;
using System.Linq.Expressions;

namespace SimpleShop.Basket.UnitTest
{
    
    public class BasketControllerTests
    {
        private readonly Mock<IRepository<Baskett>> _basketRepositoryMock;
        private readonly Mock<IRepository<BasketItem>> _basketItemRepositoryMock;
        private readonly BasketController _basketController;

        public BasketControllerTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _basketRepositoryMock = fixture.Freeze<Mock<IRepository<Baskett>>>();
            _basketItemRepositoryMock = fixture.Freeze<Mock<IRepository<BasketItem>>>();

            _basketController = new BasketController(_basketRepositoryMock.Object, _basketItemRepositoryMock.Object);
        }

        [Fact]
        public async void GetBasketByIdAsync_ShouldReturn200_IfBasketFound()
        {
            // Arrange
            _basketRepositoryMock.Setup(x => x.GetByIdAsync(It.IsIn<Guid>(basketId1))).ReturnsAsync(CreateBaskets().First(x => x.Id == basketId1));

            // Act
            var result = await _basketController.GetBasketByIdAsync(basketId1);
            var okResult = result.Result as OkObjectResult;

            // Assert
            okResult.Should().NotBe(null);
        }

        [Fact]
        public async void GetBasketByIdAsync_ShouldReturn404_IfBasketNotFound()
        {
            // Arrange
            _basketRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Baskett)null);

            // Act
            var result = await _basketController.GetBasketByIdAsync(basketId1);
            var notFoundResult = result.Result as NotFoundResult;

            // Assert
            notFoundResult.Should().NotBe(null);
            notFoundResult.StatusCode.Should().Equals(404);
        }

        [Fact]
        public async void GetBasketByUserIdAsync_ShouldReturn200_IfBasketFound()
        {
            // Arrange
            _basketRepositoryMock.Setup(x => x.GetFirstWhere(It.IsAny<Expression<Func<Baskett, bool>>>())).ReturnsAsync(CreateBaskets().First(x => x.UserId == userId1));

            // Act
            var result = await _basketController.GetBasketByUserIdAsync(userId1);
            var okResult = result.Result as OkObjectResult;

            // Assert
            okResult.Should().NotBe(null);
        }

        [Fact]
        public async void GetBasketByUserIdAsync_ShouldReturn404_IfBasketNotFound()
        {
            // Arrange
            _basketRepositoryMock.Setup(x => x.GetFirstWhere(It.IsAny<Expression<Func<Baskett, bool>>>())).ReturnsAsync((Baskett)null);

            // Act
            var result = await _basketController.GetBasketByUserIdAsync(userId1);
            var notFoundResult = result.Result as NotFoundResult;

            // Assert
            notFoundResult.Should().NotBe(null);
            notFoundResult.StatusCode.Should().Equals(404);
        }

        [Fact]
        public async void DeleteBasketAsync_ShouldReturn200_IfBasketFound()
        {
            // Arrange
            _basketRepositoryMock.Setup(x => x.GetByIdAsync(It.IsIn<Guid>(basketId1))).ReturnsAsync(CreateBaskets().First(x => x.Id == basketId1));

            // Act
            var result = await _basketController.DeleteBasketAsync(basketId1);
            var okResult = result as OkResult;

            // Assert
            okResult.Should().NotBe(null);
        }

        [Fact]
        public async void DeleteBasketAsync_ShouldReturn404_IfBasketNotFound()
        {
            // Arrange
            _basketRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Baskett)null);

            // Act
            var result = await _basketController.DeleteBasketAsync(userId1);
            var notFoundResult = result as NotFoundResult;

            // Assert
            notFoundResult.Should().NotBe(null);
            notFoundResult.StatusCode.Should().Equals(404);
        }



        private Guid basketId1 = Guid.NewGuid();
        private Guid basketId2 = Guid.NewGuid();
        private Guid userId1 = Guid.NewGuid();
        private Guid userId2 = Guid.NewGuid();

        private List<Baskett> CreateBaskets()
        {
            return new List<Baskett>()
            {
                new Baskett()
                {
                    Id = basketId1,
                    UserId = userId1,
                    DateCreated = DateTime.Now.AddDays(-10)
                },
                new Baskett()
                {
                    Id = basketId2,
                    UserId = userId2,
                    DateCreated = DateTime.Now.AddDays(-1)
                }
            };
        }
        private List<BasketItem> CreateBasketItems()
        {
            return new List<BasketItem>()
            {
                new BasketItem()
                {
                    Id = Guid.NewGuid(),
                    BasketGuid = basketId1,
                    ProductId = Guid.NewGuid(),
                    ProductName = "Test product 1",
                    Count = 2,
                    Price = 10.5M,
                    DateCreated = DateTime.Now.AddDays(-1)
                },
                new BasketItem()
                {
                    Id = Guid.NewGuid(),
                    BasketGuid = basketId1,
                    ProductId = Guid.NewGuid(),
                    ProductName = "Test product 2",
                    Count = 4,
                    Price = 50.5M,
                    DateCreated = DateTime.Now.AddDays(-1)
                },
                new BasketItem()
                {
                    Id = Guid.NewGuid(),
                    BasketGuid = basketId2,
                    ProductId = Guid.NewGuid(),
                    ProductName = "Test product 3",
                    Count = 3,
                    Price = 11.5M,
                    DateCreated = DateTime.Now.AddDays(-1)
                },
                new BasketItem()
                {
                    Id = Guid.NewGuid(),
                    BasketGuid = basketId2,
                    ProductId = Guid.NewGuid(),
                    ProductName = "Test product 4",
                    Count = 5,
                    Price = 50.5M,
                    DateCreated = DateTime.Now.AddDays(-1)
                },
            };
        }
    }
}
