﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleShop.Basket.Core.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
        public DateTime DateCreated
        {
            get
            {
                return dateCreated.HasValue ? this.dateCreated.Value : DateTime.Now;
            }
            set { this.dateCreated = value; }
        }

        private DateTime? dateCreated = null;
    }
}
