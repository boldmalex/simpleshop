﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleShop.Basket.Core.Domain
{
    public class BasketItem : BaseEntity
    {
        public Guid BasketGuid { get; set; }
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public int Count { get; set; }
        public decimal Price { get; set; }
    }
}
