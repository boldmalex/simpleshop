
FROM mcr.microsoft.com/dotnet/aspnet:3.1 AS base
WORKDIR /app
EXPOSE 5201

FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build
WORKDIR /src
COPY ["SimpleShop.Basket.WebHost/SimpleShop.Basket.WebHost.csproj", "SimpleShop.Basket.WebHost/"]
COPY ["SimpleShop.Basket.DataAccess/SimpleShop.Basket.DataAccess.csproj", "SimpleShop.Basket.DataAccess/"]
COPY ["SimpleShop.Basket.Core/SimpleShop.Basket.Core.csproj", "SimpleShop.Basket.Core/"]
RUN dotnet restore "SimpleShop.Basket.WebHost/SimpleShop.Basket.WebHost.csproj"
COPY . .
WORKDIR "/src/SimpleShop.Basket.WebHost"
RUN dotnet build "SimpleShop.Basket.WebHost.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "SimpleShop.Basket.WebHost.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "SimpleShop.Basket.WebHost.dll"]