﻿using MongoDB.Driver;
using SimpleShop.Basket.Core.Domain;
using System.Threading.Tasks;

namespace SimpleShop.Basket.DataAccess.Data
{
    public class MongoDbInitializer
        : IDbInitializer
    {
        private readonly IMongoDatabase _database;

        public MongoDbInitializer(IMongoDatabase database)
        {
            _database = database;
        }
        
        public void InitializeDb()
        {
            var basket1 = _database.GetCollection<Baskett>("Baskets").Find(x => x.Id == FakeDataFactory.basket1Id).FirstOrDefault();
            var basket2 = _database.GetCollection<Baskett>("Baskets").Find(x => x.Id == FakeDataFactory.basket2Id).FirstOrDefault();
            if (basket1 == null)
            {
                _database.GetCollection<Baskett>("Baskets").InsertMany(FakeDataFactory.Baskets);
                _database.GetCollection<BasketItem>("BasketItems").InsertMany(FakeDataFactory.BasketItems);
            }
        }
    }
}