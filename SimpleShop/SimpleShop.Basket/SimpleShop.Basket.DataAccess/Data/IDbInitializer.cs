﻿namespace SimpleShop.Basket.DataAccess.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}