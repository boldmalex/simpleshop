﻿using SimpleShop.Basket.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleShop.Basket.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static Guid basket1Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c");
        public static Guid basket2Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd");
        public static List<Baskett> Baskets => new List<Baskett>()
        {
            new Baskett()
            {
                Id = basket1Id,
                UserId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                DateCreated = DateTime.Now.AddDays(-2)
            },
            new Baskett()
            {
                Id = basket2Id,
                UserId = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                DateCreated = DateTime.Now.AddMonths(-1)
            }
        };

        public static List<BasketItem> BasketItems => new List<BasketItem>()
        {
            new BasketItem()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                BasketGuid = basket1Id,
                ProductId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                ProductName = "Тестовый товар",
                Count = 2,
                Price = 2.50M,
                DateCreated = DateTime.Now.AddDays(-1)
            }
        };
    }
}