﻿using SimpleShop.Basket.Core.Domain;
using SimpleShop.Basket.WebHost.DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleShop.Basket.WebHost.Models.Extensions
{
    public static class BasketItemExtensions
    {
        public static BasketItemResponse ToModel(this BasketItem basketItem)
        {
            return new BasketItemResponse()
            {
                BasketId = basketItem.BasketGuid,
                Count = basketItem.Count,
                Price = basketItem.Price,
                ProductId = basketItem.ProductId,
                ProductName = basketItem.ProductName
            };
        }
    }
}
