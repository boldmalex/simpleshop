﻿using SimpleShop.Basket.Core.Domain;
using SimpleShop.Basket.WebHost.DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleShop.Basket.WebHost.Models.Extensions
{
    public static class BasketExtensions
    {
        public static BasketResponse ToModel(this Baskett basket, IEnumerable<BasketItem> basketItems)
        {
            return new BasketResponse()
            {
                BasketId = basket.Id,
                UserId = basket.UserId,
                BasketItems = basketItems.Select(x => x.ToModel()).ToList()
            };
        }
    }
}
