﻿using SimpleShop.Basket.Core.Domain;
using SimpleShop.Basket.WebHost.DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleShop.Basket.WebHost.Models.Extensions
{
    public static class BasketItemRequestExtensions
    {
        public static BasketItem ToDomainBasketItem(this BasketItemRequest request, Guid basketGuid)
        {
            return new BasketItem()
            {
                BasketGuid = basketGuid,
                ProductId = request.ProductId,
                ProductName = request.ProductName,
                Count = request.Count,
                Price = request.Price
            };
        }

        public static Baskett ToDomainBasket(this Baskett request, Guid basketGuid)
        {
            return new Baskett()
            {
                Id = basketGuid,
                UserId = request.UserId
            };
        }
    }
}
