using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using MongoDB.Driver;
using Serilog;
using SimpleShop.Basket.Core.Domain;
using SimpleShop.Basket.Core.Repositories;
using SimpleShop.Basket.DataAccess.Data;
using SimpleShop.Basket.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleShop.Basket.WebHost
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddLogging(builder => builder.AddSerilog(dispose: true));

            services.AddAuthentication("Bearer")
                .AddJwtBearer("Bearer", options =>
                {
                    options.Authority = "https://localhost:5001";
                    options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters()
                    {
                        ValidateAudience = false
                    };
                });

            services.AddAuthorization();
            services.AddControllers();

            services.AddSingleton<IMongoClient>(_ => new MongoClient(Configuration["MongoDbBasket:ConnectionString"]))

                     .AddSingleton(serviceProvider => serviceProvider.GetRequiredService<IMongoClient>()
                                   .GetDatabase(Configuration["MongoDbBasket:Database"]))

                    .AddSingleton(serviceProvider => serviceProvider.GetRequiredService<IMongoDatabase>()
                                    .GetCollection<Baskett>("Baskets"))
                
                    .AddSingleton(serviceProvider => serviceProvider.GetRequiredService<IMongoDatabase>()
                                    .GetCollection<BasketItem>("BasketItems"))
                    
                    .AddScoped(serviceProvider => serviceProvider.GetRequiredService<IMongoClient>()
                                    .StartSession());


            services.AddScoped<IDbInitializer, MongoDbInitializer>();

            services.AddScoped(typeof(IRepository<>), typeof(MongoRepository<>));

            services.AddSwaggerGen();

            // TODO: ������� � ���������
            services.AddCors(options => { options.AddPolicy("AllowSpecificOrigin", builder => { builder.WithOrigins("localhost").AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod(); }); });

            services.AddSwaggerGen(options =>
            {
                options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.OAuth2,
                    Flows = new OpenApiOAuthFlows
                    {
                        Password = new OpenApiOAuthFlow
                        {
                            // TODO: ������� � ���������
                            TokenUrl = new Uri("https://localhost:5001/connect/token"),
                            Scopes = new Dictionary<string, string>
                            {
                                {"SwaggerApi", "SwaggerApi"}
                            }
                        }
                    }
                });
                options.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "oauth2"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header
                        },
                        new List<string>()
                    }
                });
            });

        }

        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            
            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "SimpleShop");
                options.OAuthClientId("swagger.client.id");
                options.OAuthClientSecret("client_swagger_secret");
            });


            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors("AllowSpecificOrigin");

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            dbInitializer.InitializeDb();
        }
    }
}
