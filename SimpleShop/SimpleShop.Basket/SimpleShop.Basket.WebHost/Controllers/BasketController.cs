﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SimpleShop.Basket.Core.Domain;
using SimpleShop.Basket.Core.Repositories;
using SimpleShop.Basket.WebHost.DTO.Models;
using SimpleShop.Basket.WebHost.Models.Extensions;
using System;
using System.Threading.Tasks;

namespace SimpleShop.Basket.WebHost.Controllers
{
    /// <summary>
    /// Корзины пользователей
    /// </summary>
    [ApiController]
    [EnableCors("AllowSpecificOrigin")]
    [Route("api/v1/[controller]")]
    [Authorize]
    public class BasketController : ControllerBase
    {
        private readonly IRepository<Baskett> _basketRepository;
        private readonly IRepository<BasketItem> _basketItemRepository;

        public BasketController(IRepository<Baskett> basketRepository
                                , IRepository<BasketItem> basketItemRepository)
        {
            _basketRepository = basketRepository;
            _basketItemRepository = basketItemRepository;
        }



        /// <summary>
        /// Получить корзину по Id
        /// </summary>
        /// <returns>Корзина</returns>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpGet("{id:guid}", Name = "GetBasket")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<BasketResponse>> GetBasketByIdAsync(Guid id)
        {
            var basket = await _basketRepository.GetByIdAsync(id);

            if (basket == null)
                return NotFound();

            var basketItems = await _basketItemRepository.GetWhere(x => x.BasketGuid == basket.Id);

            var model = basket.ToModel(basketItems);

            return Ok(model);
        }

        /// <summary>
        /// Получить корзину по Id пользователя
        /// </summary>
        /// <returns>Корзина пользователя</returns>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpGet("user/{id:guid}", Name = "GetBasketByUser")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<BasketResponse>> GetBasketByUserIdAsync(Guid id)
        {
            var basket = await _basketRepository.GetFirstWhere(x => x.UserId == id);

            if (basket == null)
                return NotFound();

            var basketItems = await _basketItemRepository.GetWhere(x => x.BasketGuid == basket.Id);

            var model = basket.ToModel(basketItems);

            return Ok(model);
        }

        /// <summary>
        /// Удалить корзину
        /// </summary>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteBasketAsync(Guid id)
        {
            // Ищем корзину
            Baskett existingBasket = await _basketRepository.GetByIdAsync(id);

            if (existingBasket == null)
            {
                return NotFound();
            }

            // Удаляем
            await _basketRepository.DeleteAsync(existingBasket);

            return Ok();
        }
    }
}
