﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SimpleShop.Basket.Core.Domain;
using SimpleShop.Basket.Core.Repositories;
using SimpleShop.Basket.WebHost.DTO.Models;
using SimpleShop.Basket.WebHost.Models.Extensions;
using System;
using System.Threading.Tasks;

namespace SimpleShop.Basket.WebHost.Controllers
{
    /// <summary>
    /// Позиции корзин пользователей
    /// </summary>
    [ApiController]
    [EnableCors("AllowSpecificOrigin")]
    [Route("api/v1/[controller]")]
    [Authorize]
    public class BasketItemController : ControllerBase
    {
        private readonly IRepository<Baskett> _basketRepository;
        private readonly IRepository<BasketItem> _basketItemRepository;

        public BasketItemController(IRepository<Baskett> basketRepository
                                , IRepository<BasketItem> basketItemRepository)
        {
            _basketRepository = basketRepository;
            _basketItemRepository = basketItemRepository;
        }

        


        /// <summary>
        /// Добавить позицию в корзину
        /// </summary>
        /// <param name="BasketItemResponse">Данные новой позиции корзины</param>
        /// <returns>Новый бренд</returns>
        /// <response code="201"> Возвращает новую позицию корзины</response>
        /// <response code="400"> Ошибка</response>  
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> PostBasketItemAsync(BasketItemRequest basketItemRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Baskett basket = await _basketRepository.GetFirstWhere(x => x.UserId == basketItemRequest.UserId);

            // Проверка наличия корзины пользователя
            if (basket == null)
            {
                basket = new Baskett() { Id = Guid.NewGuid(), UserId = basketItemRequest.UserId };
                await _basketRepository.AddAsync(basket);
            }

            // Проверка наличия позиции в корзине
            BasketItem basketItem = await _basketItemRepository.GetFirstWhere(x => x.BasketGuid == basket.Id && x.ProductId == basketItemRequest.ProductId);
            if (basketItem == null)
            {
                // Добавляем позицию
                basketItem = basketItemRequest.ToDomainBasketItem(basket.Id);
                basketItem.Id = Guid.NewGuid();
                await _basketItemRepository.AddAsync(basketItem);
            }
            else
            {
                // Изменяем кол-во в позиции
                basketItem.Count += basketItemRequest.Count;
                await _basketItemRepository.UpdateAsync(basketItem);
            }

            return Ok();
        }



        /// <summary>
        /// Изменить позицию в корзине
        /// </summary>
        /// <param name="basketItemRequest">Новые данные позиции</param>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PutBasketItemAsync(BasketItemUpdateRequest basketItemRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Ищем позицию
            var existingBasketPosition = await _basketItemRepository.GetByIdAsync(basketItemRequest.BasketItemId);

            if (existingBasketPosition == null)
            {
                return NotFound();
            }

            // Обновляем
            existingBasketPosition.Count = basketItemRequest.Count;
            await _basketItemRepository.UpdateAsync(existingBasketPosition);

            return Ok();
        }


        /// <summary>
        /// Удалить позицию корзины
        /// </summary>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteBasketItemAsync(Guid id)
        {
            // Ищем позицию
            BasketItem existingbasketItem = await _basketItemRepository.GetByIdAsync(id);

            if (existingbasketItem == null)
            {
                return NotFound();
            }

            // Удаляем
            await _basketItemRepository.DeleteAsync(existingbasketItem);

            return Ok();
        }
    }
}
