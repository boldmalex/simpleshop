﻿using System;

namespace SimpleShop.WebHost.DTO.Models.Users
{
    public class UserRoleResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}