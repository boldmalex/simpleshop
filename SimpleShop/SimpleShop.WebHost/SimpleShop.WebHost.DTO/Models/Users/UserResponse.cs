﻿using System;



namespace SimpleShop.WebHost.DTO.Models.Users
{
    public class UserResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public UserRoleResponse Role { get; set; }
    }
}