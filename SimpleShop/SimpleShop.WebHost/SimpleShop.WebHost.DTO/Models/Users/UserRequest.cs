﻿using System;


namespace SimpleShop.WebHost.DTO.Models.Users
{
    public class UserRequest
    {
        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        public Guid RoleId { get; set; }

    }
}
