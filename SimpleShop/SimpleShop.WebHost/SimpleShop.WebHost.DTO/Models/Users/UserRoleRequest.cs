﻿

namespace SimpleShop.WebHost.DTO.Models.Users
{
    public class UserRoleRequest
    {
        public string Name { get; set; }
    }
}
