﻿using System;


namespace SimpleShop.WebHost.DTO.Models.Products
{
    public class CategoryRequest
    {
        public Guid? CategoryHeadId { get; set; }
        public string Name { get; set; }
    }
}
