﻿namespace SimpleShop.WebHost.DTO.Models.Products
{
    public class BrandRequest
    {
        public string Name { get; set; }
    }
}
