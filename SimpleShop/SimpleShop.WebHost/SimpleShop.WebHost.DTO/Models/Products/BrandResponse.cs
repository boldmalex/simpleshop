﻿using System;

namespace SimpleShop.WebHost.DTO.Models.Products
{
    public class BrandResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}