﻿using System;

namespace SimpleShop.WebHost.DTO.Models.Products
{
    public class CategoryResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public CategoryShortResponse CategoryHead { get; set; }
    }
}