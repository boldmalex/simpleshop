﻿using System;
using System.Collections.Generic;


namespace SimpleShop.WebHost.DTO.Models.Products
{
    public class ProductPriceResponse
    {
        public Guid Id { get; set; }
        public ProductResponse Product { get; set; }
        public decimal PriceValue { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}