﻿using System;


namespace SimpleShop.WebHost.DTO.Models.Products
{
    public class ProductPriceRequest
    {
        public Guid ProductId { get; set; }
        public decimal PriceValue { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
