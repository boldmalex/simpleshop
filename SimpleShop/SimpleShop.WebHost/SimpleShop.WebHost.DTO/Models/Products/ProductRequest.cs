﻿using System;

namespace SimpleShop.WebHost.DTO.Models.Products
{
    public class ProductRequest
    {
        public string ProductName { get; set; }
        
        public Guid CategoryId { get; set; }

        public Guid BrandId { get; set; }

    }
}
