﻿using System;

namespace SimpleShop.WebHost.DTO.Models.Products
{
    public class CategoryShortResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}