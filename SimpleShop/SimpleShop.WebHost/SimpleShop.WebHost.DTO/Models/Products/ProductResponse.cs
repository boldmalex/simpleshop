﻿using System;


namespace SimpleShop.WebHost.DTO.Models.Products
{
    public class ProductResponse
    {
        public Guid Id { get; set; }
        public string ProductName { get; set; }
        public BrandResponse Brand { get; set; }
        public CategoryResponse Category { get; set; }
    }
}