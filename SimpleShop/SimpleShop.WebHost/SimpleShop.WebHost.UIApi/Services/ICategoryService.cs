﻿using SimpleShop.Core.Domain.Products;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SimpleShop.WebHost.DTO.Models.Products;

namespace SimpleShop.WebHost.UIApi.Services
{
    public interface ICategoryService
    {
        Task<bool> CategoryExistsAsync(Guid id);
        Task<IEnumerable<CategoryResponse>> GetAllModelsAsync();
        Task<CategoryResponse> GetModelByIdAsync(Guid id);
        Task<Category> GetDomainByIdAsync(Guid id);
        Task<CategoryResponse> AddAsync(CategoryRequest categoryRequest);
        Task UpdateAsync(Guid id, CategoryRequest categoryRequest);
        Task DeleteAsync(Guid id);
        Task<CategoryResponse> GetModelAsync(Category category);
    }
}
