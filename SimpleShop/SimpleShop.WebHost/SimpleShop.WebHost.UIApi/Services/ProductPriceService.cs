﻿
using SimpleShop.Core.Abstractions.Repositories;
using SimpleShop.Core.Domain.Products;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SimpleShop.WebHost.DTO.Models.Products;

namespace SimpleShop.WebHost.UIApi.Services
{
    public class ProductPriceService : IProductPriceService
    {

        private readonly IRepository<Price> _priceRepository;
        private readonly IProductService _productService;

        public ProductPriceService(IProductService productService,
                               IRepository<Price> priceRepository)
        {
            _productService = productService;
            _priceRepository = priceRepository;
        }


        public async Task<bool> PriceExistsAsync(Guid id)
        {
            var product = await _priceRepository.GetByIdAsync(id);

            return product != null;
        }



        public async Task<IEnumerable<ProductPriceResponse>> GetAllModelsAsync()
        {
            var result = new List<ProductPriceResponse>();
            var prices = await _priceRepository.GetAllAsync();

            foreach (var price in prices)
            {
                var priceModel = await GetModelAsync(price);
                result.Add(priceModel);
            }

            return result;
        }



        public async Task<ProductPriceResponse> GetModelByIdAsync(Guid id)
        {
            var price = await _priceRepository.GetByIdAsync(id);

            return await GetModelAsync(price);
        }



        public async Task<ProductPriceResponse> AddAsync(ProductPriceRequest productPriceRequest)
        {
            if (productPriceRequest == null)
                throw new ArgumentNullException(nameof(productPriceRequest));

            Product product = await _productService.GetDomainByIdAsync(productPriceRequest.ProductId);

            Guid newPriceGuid = Guid.NewGuid();

            var price = new Price()
            {
                Id = newPriceGuid,
                Product = product,
                PriceValue = productPriceRequest.PriceValue,
                BeginDate = productPriceRequest.BeginDate,
                EndDate = productPriceRequest.EndDate
            };
                       

            // Сохраняем
            await _priceRepository.AddAsync(price);

            // Возвращаем
            var createdPrice = await _priceRepository.GetByIdAsync(newPriceGuid);
            return await GetModelAsync(createdPrice);
        }


        
        public async Task UpdateAsync(Guid id, ProductPriceRequest productPriceRequest)
        {
            if (productPriceRequest == null)
                throw new ArgumentNullException(nameof(productPriceRequest));

            var existingProductPrice = await _priceRepository.GetByIdAsync(id);

            Product product = await _productService.GetDomainByIdAsync(productPriceRequest.ProductId);

            // Обновляем
            existingProductPrice.Product = product;
            existingProductPrice.PriceValue = productPriceRequest.PriceValue;
            existingProductPrice.BeginDate = productPriceRequest.BeginDate;
            existingProductPrice.EndDate = productPriceRequest.EndDate;

            // Сохраняем
            await _priceRepository.UpdateAsync(existingProductPrice);
        }



        public async Task DeleteAsync(Guid id)
        {
            var existingPrice = await _priceRepository.GetByIdAsync(id);

            await _priceRepository.DeleteAsync(existingPrice);
        }


        public async Task<ProductPriceResponse> GetModelAsync(Price price)
        {
            if (price == null)
                throw new ArgumentNullException(nameof(price));

            var result = new ProductPriceResponse()
            {
                Id = price.Id,
                PriceValue = price.PriceValue,
                BeginDate = price.BeginDate,
                EndDate = price.EndDate,
                Product = await _productService.GetModelAsync(price.Product)
            };

            return result;
        }
    }
}
