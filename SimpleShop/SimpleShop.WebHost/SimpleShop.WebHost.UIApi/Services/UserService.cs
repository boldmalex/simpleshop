﻿
using SimpleShop.Core.Abstractions.Repositories;
using SimpleShop.Core.Domain.Users;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SimpleShop.WebHost.UIApi.Models.Users.Extensions;
using SimpleShop.WebHost.DTO.Models.Users;

namespace SimpleShop.WebHost.UIApi.Services
{
    public class UserService : IUserService
    {

        private readonly IRepository<User> _userRepository;
        private readonly IRepository<UserRole> _userRoleRepository;

        public UserService(IRepository<User> userRepository
                           , IRepository<UserRole> userRoleRepository)
        {
            _userRepository = userRepository;
            _userRoleRepository = userRoleRepository;
        }


        public async Task<bool> UserExistsAsync(Guid id)
        {
            var user = await _userRepository.GetByIdAsync(id);

            return user != null;
        }



        public async Task<IEnumerable<UserResponse>> GetAllModelsAsync()
        {
            var result = new List<UserResponse>();
            var users = await _userRepository.GetAllAsync();

            foreach (var user in users)
            {
                var userModel = await GetModelAsync(user);
                result.Add(userModel);
            }

            return result;
        }



        public async Task<UserResponse> GetModelByIdAsync(Guid id)
        {
            var user = await _userRepository.GetByIdAsync(id);

            return await GetModelAsync(user);
        }



        public async Task<UserResponse> AddAsync(UserRequest userRequest)
        {
            if (userRequest == null)
                throw new ArgumentNullException(nameof(userRequest));

            var userRole = await _userRoleRepository.GetByIdAsync(userRequest.RoleId);
            
            Guid newUserGuid = Guid.NewGuid();

            User user = new User()
            {
                Id = newUserGuid,
                FirstName = userRequest.FirstName,
                LastName = userRequest.LastName,
                UserRole = userRole
            };
                       

            // Сохраняем
            await _userRepository.AddAsync(user);

            // Возвращаем
            var createdUser = await _userRepository.GetByIdAsync(newUserGuid);
            return await GetModelAsync(createdUser);
        }


        
        public async Task UpdateAsync(Guid id, UserRequest userRequest)
        {
            if (userRequest == null)
                throw new ArgumentNullException(nameof(userRequest));

            var existingUser = await _userRepository.GetByIdAsync(id);

            var userRole = await _userRoleRepository.GetByIdAsync(userRequest.RoleId);

            // Обновляем
            existingUser.FirstName = userRequest.FirstName;
            existingUser.LastName = userRequest.LastName;
            existingUser.UserRole = userRole;

            // Сохраняем
            await _userRepository.UpdateAsync(existingUser);
        }



        public async Task DeleteAsync(Guid id)
        {
            var existingUser = await _userRepository.GetByIdAsync(id);

            await _userRepository.DeleteAsync(existingUser);
        }

        public async Task<UserResponse> GetModelAsync(User user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            var result = new UserResponse()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Role = user.UserRole.ToModel()
            };

            return result;
        }
    }
}
