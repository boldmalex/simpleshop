﻿
using SimpleShop.Core.Abstractions.Repositories;
using SimpleShop.Core.Domain.Products;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SimpleShop.WebHost.DTO.Models.Products;

namespace SimpleShop.WebHost.UIApi.Services
{
    public class CategoryService : ICategoryService
    {

        private readonly IRepository<Category> _categoryRepository;

        public CategoryService(IRepository<Category> categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }


        public async Task<bool> CategoryExistsAsync(Guid id)
        {
            var сategory = await _categoryRepository.GetByIdAsync(id);

            return сategory != null;
        }



        public async Task<IEnumerable<CategoryResponse>> GetAllModelsAsync()
        {
            var result = new List<CategoryResponse>();
            var categories = await _categoryRepository.GetAllAsync();

            foreach (var category in categories)
            {
                var categoryModel = await GetModelAsync(category);
                result.Add(categoryModel);
            }

            return result;
        }



        public async Task<CategoryResponse> GetModelByIdAsync(Guid id)
        {
            var category = await _categoryRepository.GetByIdAsync(id);

            return await GetModelAsync(category);
        }


        public async Task<Category> GetDomainByIdAsync(Guid id)
        {
            return await _categoryRepository.GetByIdAsync(id);
        }


        public async Task<CategoryResponse> AddAsync(CategoryRequest categoryRequest)
        {
            if (categoryRequest == null)
                throw new ArgumentNullException(nameof(categoryRequest));

            Category headCategory = null;
            if (categoryRequest.CategoryHeadId.HasValue)
                headCategory = await _categoryRepository.GetByIdAsync(categoryRequest.CategoryHeadId.Value);
            
            Guid newCategoryGuid = Guid.NewGuid();

            var category = new Category()
            {
                Id = newCategoryGuid,
                CategoryName = categoryRequest.Name,
                CategoryHead = headCategory
            };
                       

            // Сохраняем
            await _categoryRepository.AddAsync(category);

            // Возвращаем
            var createdCategory = await _categoryRepository.GetByIdAsync(newCategoryGuid);
            return await GetModelAsync(createdCategory);
        }


        
        public async Task UpdateAsync(Guid id, CategoryRequest categoryRequest)
        {
            if (categoryRequest == null)
                throw new ArgumentNullException(nameof(categoryRequest));

            var existingCategory = await _categoryRepository.GetByIdAsync(id);

            Category headCategory = null;
            if (categoryRequest.CategoryHeadId.HasValue)
                headCategory = await _categoryRepository.GetByIdAsync(categoryRequest.CategoryHeadId.Value);

            // Обновляем
            existingCategory.CategoryName = categoryRequest.Name;
            existingCategory.CategoryHead = headCategory;

            // Сохраняем
            await _categoryRepository.UpdateAsync(existingCategory);
        }



        public async Task DeleteAsync(Guid id)
        {
            var existingCategory = await _categoryRepository.GetByIdAsync(id);

            await _categoryRepository.DeleteAsync(existingCategory);
        }


        public async Task<CategoryResponse> GetModelAsync(Category category)
        {
            if (category == null)
                throw new ArgumentNullException(nameof(category));

            var result = new CategoryResponse()
            {
                Id = category.Id,
                Name = category.CategoryName,
            };

            if (category.CategoryHead != null)
            {
                result.CategoryHead = new CategoryShortResponse()
                {
                    Id = category.CategoryHead.Id,
                    Name = category.CategoryHead.CategoryName
                };
            }

            return result;
        }
    }
}
