﻿
using SimpleShop.Core.Abstractions.Repositories;
using SimpleShop.Core.Domain.Products;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SimpleShop.WebHost.UIApi.Models.Products.Extensions;
using SimpleShop.WebHost.DTO.Models.Products;

namespace SimpleShop.WebHost.UIApi.Services
{
    public class ProductService : IProductService
    {

        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<Brand> _brandRepository;
        private readonly ICategoryService _categoryService;

        public ProductService(ICategoryService categoryService,
                               IRepository<Product> productRepository,
                               IRepository<Brand> brandRepository)
        {
            _categoryService = categoryService;
            _productRepository = productRepository;
            _brandRepository = brandRepository;
        }


        public async Task<bool> ProductExistsAsync(Guid id)
        {
            var product = await _productRepository.GetByIdAsync(id);

            return product != null;
        }



        public async Task<IEnumerable<ProductResponse>> GetAllModelsAsync()
        {
            var result = new List<ProductResponse>();
            var products = await _productRepository.GetAllAsync();

            foreach (var product in products)
            {
                var productModel = await GetModelAsync(product);
                result.Add(productModel);
            }

            return result;
        }



        public async Task<ProductResponse> GetModelByIdAsync(Guid id)
        {
            var product = await _productRepository.GetByIdAsync(id);

            return await GetModelAsync(product);
        }


        public async Task<Product> GetDomainByIdAsync(Guid id)
        {
            return await _productRepository.GetByIdAsync(id);
        }


        public async Task<ProductResponse> AddAsync(ProductRequest productRequest)
        {
            if (productRequest == null)
                throw new ArgumentNullException(nameof(productRequest));

            Category category = await _categoryService.GetDomainByIdAsync(productRequest.CategoryId);
            Brand brand = await _brandRepository.GetByIdAsync(productRequest.BrandId);

            Guid newProductGuid = Guid.NewGuid();

            var product = new Product()
            {
                Id = newProductGuid,
                ProductName = productRequest.ProductName,
                Category = category,
                Brand = brand
            };
                       

            // Сохраняем
            await _productRepository.AddAsync(product);

            // Возвращаем
            var createdProduct = await _productRepository.GetByIdAsync(newProductGuid);
            return await GetModelAsync(createdProduct);
        }


        
        public async Task UpdateAsync(Guid id, ProductRequest productRequest)
        {
            if (productRequest == null)
                throw new ArgumentNullException(nameof(productRequest));

            var existingProduct = await _productRepository.GetByIdAsync(id);

            Category category = await _categoryService.GetDomainByIdAsync(productRequest.CategoryId);
            Brand brand = await _brandRepository.GetByIdAsync(productRequest.BrandId);

            // Обновляем
            existingProduct.ProductName = productRequest.ProductName;
            existingProduct.Category = category;
            existingProduct.Brand = brand;

            // Сохраняем
            await _productRepository.UpdateAsync(existingProduct);
        }



        public async Task DeleteAsync(Guid id)
        {
            var existingProduct = await _productRepository.GetByIdAsync(id);

            await _productRepository.DeleteAsync(existingProduct);
        }


        public async Task<ProductResponse> GetModelAsync(Product product)
        {
            if (product == null)
                throw new ArgumentNullException(nameof(product));

            var result = new ProductResponse()
            {
                Id = product.Id,
                ProductName = product.ProductName,
                Brand = product.Brand.ToModel(),
                Category = await _categoryService.GetModelAsync(product.Category)
            };

            return result;
        }
    }
}
