﻿using SimpleShop.Core.Domain.Products;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SimpleShop.WebHost.DTO.Models.Products;

namespace SimpleShop.WebHost.UIApi.Services
{
    public interface IProductService
    {
        Task<bool> ProductExistsAsync(Guid id);
        Task<IEnumerable<ProductResponse>> GetAllModelsAsync();
        Task<ProductResponse> GetModelByIdAsync(Guid id);
        Task<Product> GetDomainByIdAsync(Guid id);
        Task<ProductResponse> AddAsync(ProductRequest productRequest);
        Task UpdateAsync(Guid id, ProductRequest productRequest);
        Task DeleteAsync(Guid id);
        Task<ProductResponse> GetModelAsync(Product product);
    }
}
