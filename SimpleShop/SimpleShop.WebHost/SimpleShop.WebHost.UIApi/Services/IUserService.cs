﻿using SimpleShop.Core.Domain.Users;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SimpleShop.WebHost.DTO.Models.Users;

namespace SimpleShop.WebHost.UIApi.Services
{
    public interface IUserService
    {
        Task<bool> UserExistsAsync(Guid id);
        Task<IEnumerable<UserResponse>> GetAllModelsAsync();
        Task<UserResponse> GetModelByIdAsync(Guid id);
        Task<UserResponse> AddAsync(UserRequest userRequest);
        Task UpdateAsync(Guid id, UserRequest userRequest);
        Task DeleteAsync(Guid id);
        Task<UserResponse> GetModelAsync(User user);
    }
}
