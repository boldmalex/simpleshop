﻿using SimpleShop.Core.Domain.Products;
using SimpleShop.WebHost.DTO.Models.Products;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SimpleShop.WebHost.UIApi.Services
{
    public interface IProductPriceService
    {
        Task<bool> PriceExistsAsync(Guid id);
        Task<IEnumerable<ProductPriceResponse>> GetAllModelsAsync();
        Task<ProductPriceResponse> GetModelByIdAsync(Guid id);
        Task<ProductPriceResponse> AddAsync(ProductPriceRequest productPriceRequest);
        Task UpdateAsync(Guid id, ProductPriceRequest productPriceRequest);
        Task DeleteAsync(Guid id);
        Task<ProductPriceResponse> GetModelAsync(Price price);
    }
}
