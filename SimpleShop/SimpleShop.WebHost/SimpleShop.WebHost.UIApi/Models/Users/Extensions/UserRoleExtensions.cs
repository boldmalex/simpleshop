﻿using SimpleShop.Core.Domain.Users;
using SimpleShop.WebHost.DTO.Models.Users;
using System;

namespace SimpleShop.WebHost.UIApi.Models.Users.Extensions
{
    public static class UserRoleExtensions
    {
        public static UserRole ToDomain(this UserRoleRequest userRoleRequest)
        {
            if (userRoleRequest == null)
                throw new ArgumentNullException(nameof(userRoleRequest));

            return new UserRole()
            {
                RoleName = userRoleRequest.Name,
            };
        }

        public static UserRoleResponse ToModel(this UserRole userRole)
        {
            if (userRole == null)
                throw new ArgumentNullException(nameof(userRole));

            return new UserRoleResponse()
            {
                Id = userRole.Id,
                Name = userRole.RoleName,
            };
        }
    }
}
