﻿using SimpleShop.Core.Domain.Products;
using SimpleShop.WebHost.DTO.Models.Products;
using System;

namespace SimpleShop.WebHost.UIApi.Models.Products.Extensions
{
    public static class BrandExtensions
    {
        public static Brand ToDomain(this BrandRequest brandRequest)
        {
            if (brandRequest == null)
                throw new ArgumentNullException(nameof(brandRequest));

            return new Brand()
            {
                BrandName = brandRequest.Name,
            };
        }

        public static BrandResponse ToModel(this Brand brand)
        {
            if (brand == null)
                throw new ArgumentNullException(nameof(brand));

            return new BrandResponse()
            {
                Id = brand.Id,
                Name = brand.BrandName,
            };
        }
    }
}
