﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SimpleShop.WebHost.UIApi.Services;
using Microsoft.AspNetCore.Cors;
using SimpleShop.WebHost.DTO.Models.Products;

namespace SimpleShop.WebHost.UIApi.Controllers
{
    /// <summary>
    /// Прайсы
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    [EnableCors("AllowSpecificOrigin")]
    public class PriceController : ControllerBase
    {
        private readonly IProductPriceService _productPriceService;
        
        public PriceController(IProductPriceService productPriceService)
        {
            _productPriceService = productPriceService;
        }

        /// <summary>
        /// Получить данные всех прайсов
        /// </summary>
        /// <returns>Товары</returns>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        [HttpGet]
        public async Task<ActionResult<List<ProductPriceResponse>>> GetPricesAsync()
        {
            var prices = await _productPriceService.GetAllModelsAsync();

            return Ok(prices.ToList());
        }


        /// <summary>
        /// Получить данные прайса по Id
        /// </summary>
        /// <returns>Прайс</returns>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpGet("{id:guid}", Name ="GetPrice")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ProductPriceResponse>> GetPriceByIdAsync(Guid id)
        {
            var price = await _productPriceService.GetModelByIdAsync(id);

            if (price == null)
                return NotFound();

            return Ok(price);
        }


        /// <summary>
        /// Добавить прайс
        /// </summary>
        /// <param name="productPriceRequest">Данные нового прайса</param>
        /// <returns>Новый прайс</returns>
        /// <response code="201"> Возвращает новый прайс</response>
        /// <response code="400"> Ошибка</response>  
        [HttpPost]
        [ProducesResponseType(typeof(ProductPriceResponse), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<ProductPriceResponse>> PostPriceAsync(ProductPriceRequest productPriceRequest)
        {
            var newPrice = await _productPriceService.AddAsync(productPriceRequest);

            return CreatedAtRoute("GetPrice", new { id = newPrice.Id }, newPrice);
        }


        /// <summary>
        /// Изменить прайс
        /// </summary>
        /// <param name="id">ИД прайса</param>
        /// <param name="productPriceRequest">Новые данные прайса</param>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpPut("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PutPriceAsync(Guid id, ProductPriceRequest productPriceRequest)
        {
            // Проверим наличие прайса
            var isExist = await _productPriceService.PriceExistsAsync(id);

            if (!isExist)
            {
                return NotFound();
            }

            // Обновляем
            await _productPriceService.UpdateAsync(id, productPriceRequest);

            return Ok();
        }


        /// <summary>
        /// Удалить прайс
        /// </summary>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeletePriceAsync(Guid id)
        {
            // Проверим наличие прайса
            var isExist = await _productPriceService.PriceExistsAsync(id);

            if (!isExist)
            {
                return NotFound();
            }

            // Удаляем
            await _productPriceService.DeleteAsync(id);

            return Ok();
        }
    }
}