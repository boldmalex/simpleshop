﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SimpleShop.Core.Abstractions.Repositories;
using SimpleShop.Core.Domain.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SimpleShop.WebHost.UIApi.Models.Products.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using SimpleShop.WebHost.DTO.Models.Products;

namespace SimpleShop.WebHost.UIApi.Controllers
{
    /// <summary>
    /// Бренды
    /// </summary>
    [ApiController]
    [EnableCors("AllowSpecificOrigin")]
    [Route("api/v1/[controller]")]
    public class BrandsController : ControllerBase
    {
        private readonly IRepository<Brand> _brandRepository;

        public BrandsController(IRepository<Brand> brandRepository)
        {
            this._brandRepository = brandRepository;
        }
        
        /// <summary>
        /// Получить все доступные бренды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<List<BrandResponse>>> GetBrandsAsync()
        {
            var brands = await _brandRepository.GetAllAsync();

            var brandsModelList = brands.Select(x => x.ToModel()).ToList();

            return Ok(brandsModelList);
        }

        /// <summary>
        /// Получить бренд по Id
        /// </summary>
        /// <returns>бренд</returns>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpGet("{id:guid}", Name = "GetBrand")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<BrandResponse>> GetBrandByIdAsync(Guid id)
        {
            var brand = await _brandRepository.GetByIdAsync(id);

            if (brand == null)
                return NotFound();

            var brandModel = brand.ToModel();

            return Ok(brandModel);
        }

        /// <summary>
        /// Добавить бренд
        /// </summary>
        /// <param name="brandRequest">Данные нового бренда</param>
        /// <returns>Новый бренд</returns>
        /// <response code="201"> Возвращает новую бренд</response>
        /// <response code="400"> Ошибка</response>  
        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(BrandResponse), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<BrandResponse>> PostBrandAsync(BrandRequest brandRequest)
        {
            Brand newBrand = brandRequest.ToDomain();

            // Добавляем бренд
            var insertedBrand = await _brandRepository.AddAsync(newBrand);

            var insertedBrandModel = insertedBrand.ToModel();

            return CreatedAtRoute("GetBrand", new {id = insertedBrandModel.Id }, insertedBrandModel);
        }


        /// <summary>
        /// Изменить бренд
        /// </summary>
        /// <param name="id">ИД бренда</param>
        /// <param name="brandRequest">Новые данные бренда</param>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpPut("{id:guid}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PutBrandAsync(Guid id, BrandRequest brandRequest)
        {
            // Ищем бренд
            var existingBrand = await _brandRepository.GetByIdAsync(id);

            if (existingBrand == null)
            {
                return NotFound();
            }

            // Обновляем
            existingBrand.BrandName = brandRequest.Name;

            await _brandRepository.UpdateAsync(existingBrand);

            return Ok();
        }


        /// <summary>
        /// Удалить бренд
        /// </summary>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpDelete("{id:guid}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteBrandAsync(Guid id)
        {
            // Ищем бренд
            var existingBrand = await _brandRepository.GetByIdAsync(id);

            if (existingBrand == null)
            {
                return NotFound();
            }

            // Удаляем
            await _brandRepository.DeleteAsync(existingBrand);

            return Ok();
        }
    }
}