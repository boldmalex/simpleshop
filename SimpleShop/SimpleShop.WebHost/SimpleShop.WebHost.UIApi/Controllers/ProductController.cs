﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SimpleShop.WebHost.UIApi.Services;
using Microsoft.AspNetCore.Cors;
using SimpleShop.WebHost.DTO.Models.Products;

namespace SimpleShop.WebHost.UIApi.Controllers
{
    /// <summary>
    /// Товары
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    [EnableCors("AllowSpecificOrigin")]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;
        
        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        /// <summary>
        /// Получить данные всех товаров
        /// </summary>
        /// <returns>Товары</returns>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        [HttpGet]
        public async Task<ActionResult<List<ProductResponse>>> GetProductsAsync()
        {
            var products = await _productService.GetAllModelsAsync();

            return Ok(products.ToList());
        }


        /// <summary>
        /// Получить данные товара по Id
        /// </summary>
        /// <returns>Товар</returns>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpGet("{id:guid}", Name ="GetProduct")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ProductResponse>> GetProductByIdAsync(Guid id)
        {
            var product = await _productService.GetModelByIdAsync(id);

            if (product == null)
                return NotFound();

            return Ok(product);
        }


        /// <summary>
        /// Добавить товар
        /// </summary>
        /// <param name="productRequest">Данные нового товара</param>
        /// <returns>Новый товар</returns>
        /// <response code="201"> Возвращает новый товар</response>
        /// <response code="400"> Ошибка</response>  
        [HttpPost]
        [ProducesResponseType(typeof(ProductResponse), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<ProductResponse>> PostProductAsync(ProductRequest productRequest)
        {
            var newProduct = await _productService.AddAsync(productRequest);

            return CreatedAtRoute("GetProduct", new { id = newProduct.Id }, newProduct);
        }


        /// <summary>
        /// Изменить товар
        /// </summary>
        /// <param name="id">ИД товара</param>
        /// <param name="productRequest">Новые данные товара</param>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpPut("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PutProductAsync(Guid id, ProductRequest productRequest)
        {
            // Проверим наличие товара
            var isExist = await _productService.ProductExistsAsync(id);

            if (!isExist)
            {
                return NotFound();
            }

            // Обновляем
            await _productService.UpdateAsync(id, productRequest);

            return Ok();
        }


        /// <summary>
        /// Удалить товар
        /// </summary>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteProductAsync(Guid id)
        {
            // Проверим наличие товара
            var isExist = await _productService.ProductExistsAsync(id);

            if (!isExist)
            {
                return NotFound();
            }

            // Удаляем
            await _productService.DeleteAsync(id);

            return Ok();
        }
    }
}