﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SimpleShop.Core.Abstractions.Repositories;
using SimpleShop.Core.Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SimpleShop.WebHost.UIApi.Models.Users.Extensions;
using Microsoft.AspNetCore.Cors;
using SimpleShop.WebHost.DTO.Models.Users;

namespace SimpleShop.WebHost.UIApi.Controllers
{
    /// <summary>
    /// Роли
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    [EnableCors("AllowSpecificOrigin")]
    public class UserRolesController : ControllerBase
    {
        private readonly IRepository<UserRole> _userRoleRepository;

        public UserRolesController(IRepository<UserRole> userRoleRepository)
        {
            this._userRoleRepository = userRoleRepository;
        }
        
        /// <summary>
        /// Получить все доступные роли
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<UserRoleResponse>>> GetRolesAsync()
        {
            var roles = await _userRoleRepository.GetAllAsync();

            var rolesModelList = roles.Select(x => x.ToModel()).ToList();

            return Ok(rolesModelList);
        }

        /// <summary>
        /// Получить роль по Id
        /// </summary>
        /// <returns>Роль</returns>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpGet("{id:guid}", Name = "GetRole")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<UserRoleResponse>> GetRoleByIdAsync(Guid id)
        {
            var role = await _userRoleRepository.GetByIdAsync(id);

            if (role == null)
                return NotFound();

            var roleModel = role.ToModel();

            return Ok(roleModel);
        }

        /// <summary>
        /// Добавить роль
        /// </summary>
        /// <param name="roleRequest">Данные новой роли</param>
        /// <returns>Новая роль</returns>
        /// <response code="201"> Возвращает новую роль</response>
        /// <response code="400"> Ошибка</response>  
        [HttpPost]
        [ProducesResponseType(typeof(UserRoleResponse), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<UserRoleResponse>> PostRoleAsync(UserRoleRequest roleRequest)
        {
            UserRole newRole = roleRequest.ToDomain();

            // Добавляем роль
            var insertedRole = await _userRoleRepository.AddAsync(newRole);

            var insertedRoleModel = insertedRole.ToModel();

            return CreatedAtRoute("GetRole", new {id = insertedRoleModel.Id }, insertedRoleModel);
        }


        /// <summary>
        /// Изменить роль
        /// </summary>
        /// <param name="id">ИД роли</param>
        /// <param name="roleRequest">Новые данные роли</param>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpPut("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PutRoleAsync(Guid id, UserRoleRequest roleRequest)
        {
            // Ищем роль
            var existingRole = await _userRoleRepository.GetByIdAsync(id);

            if (existingRole == null)
            {
                return NotFound();
            }

            // Новая роль
            var newRole = roleRequest.ToDomain();

            await _userRoleRepository.UpdateAsync(newRole);

            return Ok();
        }


        /// <summary>
        /// Удалить роль
        /// </summary>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteRoleAsync(Guid id)
        {
            // Ищем роль
            var existingRole = await _userRoleRepository.GetByIdAsync(id);

            if (existingRole == null)
            {
                return NotFound();
            }

            // Удаляем
            await _userRoleRepository.DeleteAsync(existingRole);

            return Ok();
        }
    }
}