﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SimpleShop.WebHost.UIApi.Services;
using Microsoft.AspNetCore.Cors;
using SimpleShop.WebHost.DTO.Models.Products;

namespace SimpleShop.WebHost.UIApi.Controllers
{
    /// <summary>
    /// Категории товаров
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    [EnableCors("AllowSpecificOrigin")]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;
        
        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        /// <summary>
        /// Получить данные всех категорий
        /// </summary>
        /// <returns>Категории</returns>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        [HttpGet]
        public async Task<ActionResult<List<CategoryResponse>>> GetCategoriesAsync()
        {
            var categories = await _categoryService.GetAllModelsAsync();

            return Ok(categories.ToList());
        }


        /// <summary>
        /// Получить данные категрии по Id
        /// </summary>
        /// <returns>Категория</returns>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpGet("{id:guid}", Name ="GetCategory")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CategoryResponse>> GetCategoryByIdAsync(Guid id)
        {
            var category = await _categoryService.GetModelByIdAsync(id);

            if (category == null)
                return NotFound();

            return Ok(category);
        }


        /// <summary>
        /// Добавить категорию
        /// </summary>
        /// <param name="categoryRequest">Данные нового пользователя</param>
        /// <returns>Новый пользователь</returns>
        /// <response code="201"> Возвращает нового пользователя</response>
        /// <response code="400"> Ошибка</response>  
        [HttpPost]
        [ProducesResponseType(typeof(CategoryResponse), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<CategoryResponse>> PostCategoryAsync(CategoryRequest categoryRequest)
        {
            var newCategory = await _categoryService.AddAsync(categoryRequest);

            return CreatedAtRoute("GetCategory", new { id = newCategory.Id }, newCategory);
        }


        /// <summary>
        /// Изменить категорию
        /// </summary>
        /// <param name="id">ИД категории</param>
        /// <param name="categoryRequest">Новые данные категории</param>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpPut("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PutCategoryAsync(Guid id, CategoryRequest categoryRequest)
        {
            // Проверим наличие категории
            var isExist = await _categoryService.CategoryExistsAsync(id);

            if (!isExist)
            {
                return NotFound();
            }

            // Обновляем
            await _categoryService.UpdateAsync(id, categoryRequest);

            return Ok();
        }


        /// <summary>
        /// Удалить категорию
        /// </summary>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteCategoryAsync(Guid id)
        {
            // Проверим наличие категории
            var isExist = await _categoryService.CategoryExistsAsync(id);

            if (!isExist)
            {
                return NotFound();
            }

            // Удаляем
            await _categoryService.DeleteAsync(id);

            return Ok();
        }
    }
}