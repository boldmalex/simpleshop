﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SimpleShop.WebHost.UIApi.Services;
using Microsoft.AspNetCore.Cors;
using SimpleShop.WebHost.DTO.Models.Users;

namespace SimpleShop.WebHost.UIApi.Controllers
{
    /// <summary>
    /// Пользоваетли
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    [EnableCors("AllowSpecificOrigin")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        
        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Получить данные пользователей
        /// </summary>
        /// <returns>Клиенты</returns>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        [HttpGet]
        public async Task<ActionResult<List<UserResponse>>> GetUsersAsync()
        {
            var users = await _userService.GetAllModelsAsync();

            return Ok(users.ToList());
        }


        /// <summary>
        /// Получить данные пользователя по Id
        /// </summary>
        /// <returns>Пользователь</returns>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpGet("{id:guid}", Name ="GetUser")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<UserResponse>> GetUserByIdAsync(Guid id)
        {
            var user = await _userService.GetModelByIdAsync(id);

            if (user == null)
                return NotFound();

            return Ok(user);
        }


        /// <summary>
        /// Добавить пользователя
        /// </summary>
        /// <param name="userRequest">Данные нового пользователя</param>
        /// <returns>Новый пользователь</returns>
        /// <response code="201"> Возвращает нового пользователя</response>
        /// <response code="400"> Ошибка</response>  
        [HttpPost]
        [ProducesResponseType(typeof(UserResponse), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<UserResponse>> PostUserAsync(UserRequest userRequest)
        {
            var newUser = await _userService.AddAsync(userRequest);

            return CreatedAtRoute("GetUser", new { id = newUser.Id }, newUser);
        }


        /// <summary>
        /// Изменить сотрудника
        /// </summary>
        /// <param name="id">ИД сотрудника</param>
        /// <param name="userRequest">Новые данные сотрудника</param>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpPut("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PutUserAsync(Guid id, UserRequest userRequest)
        {
            // Проверим наличие клиента
            var isExist = await _userService.UserExistsAsync(id);

            if (!isExist)
            {
                return NotFound();
            }

            // Обновляем
            await _userService.UpdateAsync(id, userRequest);

            return Ok();
        }


        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteUserAsync(Guid id)
        {
            // Проверим наличие клиента
            var isExist = await _userService.UserExistsAsync(id);

            if (!isExist)
            {
                return NotFound();
            }

            // Удаляем
            await _userService.DeleteAsync(id);

            return Ok();
        }
    }
}