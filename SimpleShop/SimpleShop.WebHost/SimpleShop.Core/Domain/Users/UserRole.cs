﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleShop.Core.Domain.Users
{
    public class UserRole : BaseEntity
    {
        public string RoleName { get; set; }
    }
}
