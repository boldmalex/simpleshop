﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleShop.Core.Domain.Products
{
    public class Price : BaseEntity
    {
        public decimal PriceValue { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public virtual Product Product { get; set; }
    }
}
