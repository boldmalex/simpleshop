﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleShop.Core.Domain.Products
{
    public class Category : BaseEntity
    {
        public virtual Category CategoryHead { get; set; }
        public string CategoryName { get; set; }
    }
}
