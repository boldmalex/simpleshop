﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleShop.Core.Domain.Products
{
    public class Product : BaseEntity
    {
        public virtual Category Category { get; set; }
        public virtual Brand Brand { get; set; }
        public string ProductName { get; set; }
    }
}
