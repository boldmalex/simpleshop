﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleShop.Core.Domain.Products
{
    public class Brand : BaseEntity
    {
        public string BrandName { get; set; }

    }
}
