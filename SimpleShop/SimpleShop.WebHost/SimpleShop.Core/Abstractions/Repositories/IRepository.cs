﻿using SimpleShop.Core.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SimpleShop.Core.Abstractions.Repositories
{
    public interface IRepository<T> where T: BaseEntity
    {
        Task<List<T>> GetAllAsync();

        Task<T> GetByIdAsync(Guid id);

        Task<List<T>> GetByIdsAsync(List<Guid> ids);

        Task<T> AddAsync(T entity);

        Task UpdateAsync(T entity);

        Task DeleteAsync(T entity);
    }
}
