﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleShop.DataAccessEF.Data
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}
