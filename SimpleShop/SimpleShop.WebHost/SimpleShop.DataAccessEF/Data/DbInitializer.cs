﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleShop.DataAccessEF.Data
{
    public class DbInitializer : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public DbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            //тестовые данные
            DataInit();
            DbAddBrands();
            DbAddCategories();
            DbAddProducts();
            _dataContext.SaveChanges();
        }


        #region Data

        private Core.Domain.Products.Brand BrandOtus;
        private Core.Domain.Products.Brand BrandMicrosoft;
        private Core.Domain.Products.Category CategoryPo;
        private Core.Domain.Products.Category CategoryCourses;
        private Core.Domain.Products.Category CategoryOS;
        private Core.Domain.Products.Category CategoryForBusiness;
        private Core.Domain.Products.Category CategoryForCoders;

        private void DataInit()
        {
            BrandOtus = new Core.Domain.Products.Brand()
            {
                Id = Guid.NewGuid(),
                BrandName = "OTUS"
            };
            BrandMicrosoft = new Core.Domain.Products.Brand()
            {
                Id = Guid.NewGuid(),
                BrandName = "Microsoft"
            };
            CategoryPo = new Core.Domain.Products.Category()
            {
                Id = Guid.NewGuid(),
                CategoryName = "ПО"
            };
            CategoryCourses = new Core.Domain.Products.Category()
            {
                Id = Guid.NewGuid(),
                CategoryName = "Курсы"
            };
            CategoryOS = new Core.Domain.Products.Category()
            {
                Id = Guid.NewGuid(),
                CategoryName = "Операционные системы",
                CategoryHead = CategoryPo
            };

            CategoryForBusiness = new Core.Domain.Products.Category()
            {
                Id = Guid.NewGuid(),
                CategoryName = "Для бизнеса",
                CategoryHead = CategoryPo
            };

            CategoryForCoders = new Core.Domain.Products.Category()
            {
                Id = Guid.NewGuid(),
                CategoryName = "Для разработчиков",
                CategoryHead = CategoryPo
            };
        }

        private void DbAddBrands()
        {
            _dataContext.Brands.AddRange(BrandOtus, BrandMicrosoft);
        }

        private void DbAddCategories()
        {
            _dataContext.Categories.AddRange(CategoryPo, CategoryOS, CategoryCourses, CategoryForBusiness, CategoryForCoders);
        }

        private void DbAddProducts()
        {
            _dataContext.Products.AddRange(
                new Core.Domain.Products.Product()
                { 
                    Brand = BrandMicrosoft,
                    Category = CategoryOS,
                    ProductName = "WIN 10",
                    Id = Guid.NewGuid(),
                },
                new Core.Domain.Products.Product()
                {
                    Brand = BrandMicrosoft,
                    Category = CategoryOS,
                    ProductName = "WIN 11",
                    Id = Guid.NewGuid(),
                },
                new Core.Domain.Products.Product()
                {
                    Brand = BrandMicrosoft,
                    Category = CategoryOS,
                    ProductName = "WIN 7",
                    Id = Guid.NewGuid(),
                },
                new Core.Domain.Products.Product()
                {
                    Brand = BrandMicrosoft,
                    Category = CategoryForBusiness,
                    ProductName = "OFFICE 365",
                    Id = Guid.NewGuid(),
                },
                new Core.Domain.Products.Product()
                {
                    Brand = BrandMicrosoft,
                    Category = CategoryForCoders,
                    ProductName = "Visual studio 2019",
                    Id = Guid.NewGuid(),
                },
                new Core.Domain.Products.Product()
                {
                    Brand = BrandMicrosoft,
                    Category = CategoryForCoders,
                    ProductName = "Visual studio 2021",
                    Id = Guid.NewGuid(),
                },
                new Core.Domain.Products.Product()
                {
                    Brand = BrandOtus,
                    Category = CategoryCourses,
                    ProductName = "Asp.Net разработчик",
                    Id = Guid.NewGuid(),
                },
                new Core.Domain.Products.Product()
                {
                    Brand = BrandOtus,
                    Category = CategoryCourses,
                    ProductName = "Asp.Net Core разработчик",
                    Id = Guid.NewGuid(),
                }

                );
        }

        #endregion data
    }
}
