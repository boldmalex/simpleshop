﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ValueGeneration;
using SimpleShop.Core.Domain.Products;
using SimpleShop.Core.Domain.Users;

namespace SimpleShop.DataAccessEF
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {}


        #region Users
        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        #endregion Users


        #region Products
        public DbSet<Category> Categories { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Price> Prices { get; set; }
        #endregion Products


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Users
            modelBuilder.Entity<User>().HasKey(u => u.Id);
            modelBuilder.Entity<User>().Property(u => u.Id).ValueGeneratedOnAdd().HasValueGenerator<GuidValueGenerator>().IsRequired();
            modelBuilder.Entity<User>().Property(u => u.FirstName).IsRequired();
            modelBuilder.Entity<User>().Property(u => u.LastName).IsRequired();
            modelBuilder.Entity<User>().HasOne(u=>u.UserRole).WithMany().IsRequired();

            modelBuilder.Entity<UserRole>().HasKey(ur => ur.Id);
            modelBuilder.Entity<UserRole>().Property(u => u.Id).ValueGeneratedOnAdd().HasValueGenerator<GuidValueGenerator>().IsRequired();
            modelBuilder.Entity<UserRole>().Property(ur => ur.RoleName).IsRequired();
            #endregion Users


            #region Products
            modelBuilder.Entity<Brand>().HasKey(b => b.Id);
            modelBuilder.Entity<Brand>().Property(b => b.Id).ValueGeneratedOnAdd().HasValueGenerator<GuidValueGenerator>().IsRequired();
            modelBuilder.Entity<Brand>().Property(b => b.BrandName).IsRequired();

            modelBuilder.Entity<Category>().HasKey(c => c.Id);
            modelBuilder.Entity<Category>().Property(c => c.Id).ValueGeneratedOnAdd().HasValueGenerator<GuidValueGenerator>().IsRequired();
            modelBuilder.Entity<Category>().Property(c => c.CategoryName).IsRequired();
            modelBuilder.Entity<Category>().HasOne(c => c.CategoryHead).WithMany();

            modelBuilder.Entity<Product>().HasKey(p => p.Id);
            modelBuilder.Entity<Product>().Property(p => p.Id).ValueGeneratedOnAdd().HasValueGenerator<GuidValueGenerator>().IsRequired();
            modelBuilder.Entity<Product>().Property(p => p.ProductName).IsRequired();
            modelBuilder.Entity<Product>().HasOne(p => p.Category).WithMany().IsRequired();
            modelBuilder.Entity<Product>().HasOne(p => p.Brand).WithMany().IsRequired();

            modelBuilder.Entity<Price>().HasKey(p => p.Id);
            modelBuilder.Entity<Price>().Property(p => p.Id).ValueGeneratedOnAdd().HasValueGenerator<GuidValueGenerator>().IsRequired();
            modelBuilder.Entity<Price>().Property(p => p.PriceValue).IsRequired().HasDefaultValue(0);
            modelBuilder.Entity<Price>().HasOne(p => p.Product).WithMany().IsRequired();

            #endregion Products
        }
    }
}
