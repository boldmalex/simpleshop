﻿using SimpleShop.Core.Abstractions.Repositories;
using SimpleShop.Core.Domain;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace SimpleShop.DataAccessEF.Repositories
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {

        private readonly DataContext dataContext;

        public Repository(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public async Task<T> AddAsync(T entity)
        {
            var result = await dataContext.AddAsync<T>(entity);
            await dataContext.SaveChangesAsync();
            return result.Entity;
        }

        public async Task DeleteAsync(T entity)
        {
            dataContext.Remove<T>(entity);
            await dataContext.SaveChangesAsync();
        }

        public async Task<List<T>> GetAllAsync()
        {
            var result = await dataContext.Set<T>().ToListAsync();
            return result;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var result = await dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            return result;
        }

        public async Task<List<T>> GetByIdsAsync(List<Guid> ids)
        {
            var result = await dataContext.Set<T>().Where(x => ids.Contains(x.Id)).ToListAsync();
            return result;
        }

        public async Task UpdateAsync(T entity)
        {
            await dataContext.SaveChangesAsync();
        }
    }
}
