using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using SimpleShop.Basket.WebHost.DTO.Models;
using SimpleShop.WebHost.UIShop.Services.HttpClients;

namespace SimpleShop.WebHost.UIShop.Pages.Account.Manage
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IHttpClientBasket _httpClientBasket;

        public IndexModel(ILogger<IndexModel> logger, IHttpClientBasket httpClientBasket)
        {
            _logger = logger;
            _httpClientBasket = httpClientBasket;
        }

        public BasketResponse Basket { get; set; }


        public async void OnGet()
        {
            var userId = Guid.Parse(User.Claims.First(x => x.Type == "sub").Value);
            var res = await _httpClientBasket.GetBasketByUserIdAsync(userId);

            _logger.LogInformation($"Get basket response: {res}");
        }
    }
}
