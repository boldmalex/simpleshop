﻿using SimpleShop.Basket.WebHost.DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleShop.WebHost.UIShop.Services.HttpClients
{
    public interface IHttpClientBasket
    {
        Task<BasketResponse> GetBasketByIdAsync(Guid id);
        Task<BasketResponse> GetBasketByUserIdAsync(Guid id);
        Task DeleteBasketAsync(Guid id);
        Task PostBasketItemAsync(BasketItemRequest basketItemRequest);
        Task PutBasketItemAsync(BasketItemUpdateRequest basketItemRequest);
        Task DeleteBasketItemAsync(Guid id);
    }
}
