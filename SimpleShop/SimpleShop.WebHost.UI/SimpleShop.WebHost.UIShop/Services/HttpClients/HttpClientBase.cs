﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace SimpleShop.WebHost.UIShop.Services.HttpClients
{
    public class HttpClientBase : IDisposable
    {
        protected HttpClient _httpClient;

        public HttpClientBase(Uri clientServiceUrl)
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = clientServiceUrl;
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public void Dispose()
        {
            _httpClient.Dispose();
        }
    }
}
