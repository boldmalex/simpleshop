﻿using IdentityModel.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SimpleShop.Basket.WebHost.DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace SimpleShop.WebHost.UIShop.Services.HttpClients
{
    public class HttpClientBasket : HttpClientBase, IHttpClientBasket
    {

        private readonly ILogger<HttpClientBasket> _logger;
        private IConfiguration _configuration;

        public HttpClientBasket(IConfiguration configration, ILogger<HttpClientBasket> logger) : base(new Uri(configration["Services:BasketServiceUrl"]))
        {
            _logger = logger;
            _configuration = configration;
            Init();
        }

        private void Init()
        {
            var client = new HttpClient();

            var disco = client.GetDiscoveryDocumentAsync(_configuration["IdentityService:IdentityServiceUrl"]).GetAwaiter().GetResult();

            if (disco.IsError)
            {
                Console.WriteLine(disco.Error);
            }

            var tokenResponse = client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = _configuration["IdentityService:IdentityServiceClientId"],
                ClientSecret = _configuration["IdentityService:IdentityServiceClientSecret"],
                Scope = _configuration["IdentityService:IdentityServiceScope"]
            }).GetAwaiter().GetResult();

            if (tokenResponse.IsError)
            {
                _logger.LogError(tokenResponse.Error);
            }

            _logger.LogInformation($"Getted bearer: {tokenResponse.AccessToken}");

            _httpClient.SetBearerToken(tokenResponse.AccessToken);
        }

        public Task DeleteBasketAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task DeleteBasketItemAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<BasketResponse> GetBasketByIdAsync(Guid id)
        {
            return await _httpClient.GetFromJsonAsync<BasketResponse>($"api/v1/Basket/{id.ToString()}");
        }

        public Task<BasketResponse> GetBasketByUserIdAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task PostBasketItemAsync(BasketItemRequest basketItemRequest)
        {
            throw new NotImplementedException();
        }

        public Task PutBasketItemAsync(BasketItemUpdateRequest basketItemRequest)
        {
            throw new NotImplementedException();
        }
    }
}
