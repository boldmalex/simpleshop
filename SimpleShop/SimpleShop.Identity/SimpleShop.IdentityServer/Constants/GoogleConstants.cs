﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleShop.IdentityServer.Constants
{
    public class GoogleConstants
    {
        public const string constClaimName = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name";
        public const string constClaimEmail = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress";
    }
}
