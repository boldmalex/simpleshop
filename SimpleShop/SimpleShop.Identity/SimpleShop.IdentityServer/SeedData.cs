﻿
using System;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using IdentityServer4.EntityFramework.Storage;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityModel;
using Microsoft.AspNetCore.Identity;
using SimpleShop.IdentityServer.Data;

namespace SimpleShop.IdentityServer
{
    public class SeedData
    {
        public static void EnsureSeedData(string connectionString)
        {
            var services = new ServiceCollection();
            
            services.AddOperationalDbContext(options =>
            {
                options.ConfigureDbContext = db => db.UseSqlite(connectionString, sql => sql.MigrationsAssembly(typeof(SeedData).Assembly.FullName));
            });
            
            services.AddConfigurationDbContext(options =>
            {
                options.ConfigureDbContext = db => db.UseSqlite(connectionString, sql => sql.MigrationsAssembly(typeof(SeedData).Assembly.FullName));
            });
            
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlite(connectionString, sql => sql.MigrationsAssembly(typeof(SeedData).Assembly.FullName));
            });
            
            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddLogging();


            var serviceProvider = services.BuildServiceProvider();


            using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                scope.ServiceProvider.GetService<PersistedGrantDbContext>().Database.Migrate();
                scope.ServiceProvider.GetService<ConfigurationDbContext>().Database.Migrate();
                scope.ServiceProvider.GetService<ApplicationDbContext>().Database.Migrate();
                                
                var context = scope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();
                EnsureSeedData(context);
                EnsureUsers(scope);
            }
        }

        private static void EnsureSeedData(ConfigurationDbContext context)
        {
            if (!context.Clients.Any())
            {
                Log.Debug("Clients being populated");
                foreach (var client in Config.Clients.ToList())
                {
                    context.Clients.Add(client.ToEntity());
                }
                context.SaveChanges();
            }
            else
            {
                Log.Debug("Clients already populated");
            }

            if (!context.IdentityResources.Any())
            {
                Log.Debug("IdentityResources being populated");
                foreach (var resource in Config.IdentityResources.ToList())
                {
                    context.IdentityResources.Add(resource.ToEntity());
                }
                context.SaveChanges();
            }
            else
            {
                Log.Debug("IdentityResources already populated");
            }

            if (!context.ApiScopes.Any())
            {
                Log.Debug("ApiScopes being populated");
                foreach (var resource in Config.ApiScopes.ToList())
                {
                    context.ApiScopes.Add(resource.ToEntity());
                }
                context.SaveChanges();
            }
            else
            {
                Log.Debug("ApiScopes already populated");
            }
        }

        private static void EnsureUsers(IServiceScope scope)
        {
            var adminName = "ShopAdmin";

            var userManager = scope.ServiceProvider.GetRequiredService<UserManager<IdentityUser>>();

            var admin = userManager.FindByNameAsync((adminName)).Result;

            if (admin == null)
            {
                admin = new IdentityUser
                {
                    UserName = adminName,
                    Email = "boldmalex@gmail.com",
                    EmailConfirmed = true
                };


                var createResult = userManager.CreateAsync(admin, "passw0rd_ShoP$");

                var claimResult = userManager.AddClaimsAsync(admin, new Claim[]
                {
                    new Claim(JwtClaimTypes.Name, "Super Admin"),
                    new Claim(JwtClaimTypes.Role, "Admin")
                }).Result;

                Log.Debug("Admin already populated");
            }

        }
    }
}
